// the game itself
var game;

var gameOptions = {

    // slices (prizes) placed in the wheel
    // slices: 12,

    // prize names, starting from 12 o'clock going clockwise
    slicePrizes: ["20", "3Spin", "6", "1Spin", "10", "1000"],

    // wheel rotation duration, in milliseconds
    rotationTime: 3000
}

// once the window loads...
window.onload = function() {

    // game configuration object
    var gameConfig = {

        // render type
        type: Phaser.CANVAS,

        // game width, in pixels
        width: 550,

        // game height, in pixels
        height: 550,

        // game background color
        // backgroundColor: 0x000000,
        transparent: true,

        // scenes used by the game
        scene: [playGame]
    };

    // game constructor
    game = new Phaser.Game(gameConfig);

    // pure javascript to give focus to the page/frame and scale the game
    window.focus()
    resize();
    window.addEventListener("resize", resize, false);
}

function spinFromOutside() {
    game.scene.scenes[0].spinWheel();
}


// PlayGame scene
class playGame extends Phaser.Scene {

    // constructor
    constructor() {
        super("PlayGame");
        Phaser.Scene.call(this, { key: 'sceneA' });
    }

    // method to be executed when the scene preloads
    preload() {
        // loading assets
        this.load.image("wheel", "wheel.png");
        this.load.image("pin", "pin.png");
    }

    // method to be executed once the scene has been created
    create() {

        // adding the wheel in the middle of the canvas
        this.wheel = this.add.sprite(game.config.width / 2, game.config.height / 2, "wheel");
        this.wheel.scaleX = 0.25;
        this.wheel.scaleY = this.wheel.scaleX;

        // adding the pin in the middle of the canvas
        this.pin = this.add.sprite(game.config.width / 2, game.config.height / 2 - 15, "pin");
        this.pin.scaleX = 0.25;
        this.pin.scaleY = this.pin.scaleX;

        // // adding the text field
        // this.prizeText = this.add.text(game.config.width / 2, game.config.height - 20, "Spin the wheel", {
        //     font: "bold 32px Arial",
        //     align: "center",
        //     color: "white"
        // });

        // center the text
        // this.prizeText.setOrigin(0.5);

        // the game has just started = we can spin the wheel
        this.canSpin = true;

        // waiting for your input, then calling "spinWheel" function
        // this.input.on("pointerdown", this.spinWheel, this);
    }

    randomOffset() {
        return Phaser.Math.Between(10, 50);
    }

    spinPercent() {
        var randomInt = Phaser.Math.Between(0, 1000);
        if (randomInt == 0) {
            return 0
        } else if (randomInt >= 100 && randomInt < 140) {
            return 4
        } else if (randomInt >= 200 && randomInt < 230) {
            return 5
        } else if (randomInt >= 300 && randomInt < 500) {
            return 1
        } else if (randomInt >= 600 && randomInt < 870) {
            return 2
        } else {
            return 3
        }
    }

    // function to spin the wheel
    spinWheel() {

        // can we spin the wheel?
        if (this.canSpin) {

            // resetting text field
            // this.prizeText.setText("");

            // the wheel will spin round from 2 to 4 times. This is just coreography
            var rounds = Phaser.Math.Between(5, 10);

            // then will rotate by a random number from 0 to 360 degrees. This is the actual spin
            var getSpinResult = this.spinPercent();
            var offset = this.randomOffset();
            var degrees = getSpinResult * 60 + offset;
            // console.log(degrees);
            // console.log(getSpinResult);
            // console.log(offset);
            // before the wheel ends spinning, we already know the prize according to "degrees" rotation and the number of slices
            var prize = gameOptions.slicePrizes.length - 1 - Math.floor(degrees / (360 / gameOptions.slicePrizes.length));
            console.log(prize);
            // now the wheel cannot spin because it's already spinning
            this.canSpin = false;

            // animation tweeen for the spin: duration 3s, will rotate by (360 * rounds + degrees) degrees
            // the quadratic easing will simulate friction
            this.tweens.add({

                // adding the wheel to tween targets
                targets: [this.wheel],

                // angle destination
                angle: 360 * rounds + degrees,

                // tween duration
                duration: gameOptions.rotationTime,

                // tween easing
                ease: "Cubic.easeOut",

                // callback scope
                callbackScope: this,

                // function to be executed once the tween has been completed
                onComplete: function(tween) {

                    // displaying prize text
                    // this.prizeText.setText(gameOptions.slicePrizes[prize]);

                    // player can spin again
                    this.canSpin = true;
                    this.updatePrize(gameOptions.slicePrizes[prize]);

                }
            });
        }
    }

    updatePrize(prize) {
        // Sound List: 1spin, 3spin, BGM1, BGM2, BGM3, wheelSpin, reward7, reward10, reward20, reward1000
        // Image List: GameBagMoney, GameGiftBox, GameGreenCycle, GameJackpot, GameTornado, GameTrophy
        var body = {}
        if (prize == "1Spin") {
            body["spin"] = 1;
            body["gem"] = 0;
            body["sound"] = "1spin";
            body["image"] = "GameGreenCycle";
            body["title"] = "Win";
            body["animateCount"] = 0
        } else if (prize == "3Spin") {
            body["spin"] = 3;
            body["gem"] = 0;
            body["sound"] = "3spin";
            body["image"] = "GameTornado";
            body["title"] = "Win";
            body["animateCount"] = 0
        } else {
            body["spin"] = 0;
            var gemCount = parseInt(prize);
            body["gem"] = gemCount;
            if (gemCount == 6) {
                body["sound"] = "reward7";
                body["image"] = "GameGiftBox";
                body["title"] = "Win";
                body["animateCount"] = 1
            } else if (gemCount == 10) {
                body["sound"] = "reward10";
                body["image"] = "GameBagMoney";
                body["title"] = "Win";
                body["animateCount"] = 2
            } else if (gemCount == 20) {
                body["sound"] = "reward20";
                body["image"] = "GameTrophy";
                body["title"] = "Win";
                body["animateCount"] = 3
            } else {
                body["sound"] = "reward1000";
                body["image"] = "GameJackpot";
                body["title"] = "Jackpot!";
                body["animateCount"] = 30
            }
        }
        try {
            webkit.messageHandlers.updatePrize.postMessage(body);
        } catch (err) {
            // this.prizeText.setText(err);
        }
    }
}

// pure javascript to scale the game
function resize() {
    var canvas = document.querySelector("canvas");
    var windowWidth = window.innerWidth;
    var windowHeight = window.innerHeight;
    var windowRatio = windowWidth / windowHeight;
    var gameRatio = game.config.width / game.config.height;
    if (windowRatio < gameRatio) {
        canvas.style.width = windowWidth + "px";
        canvas.style.height = (windowWidth / gameRatio) + "px";
    } else {
        canvas.style.width = (windowHeight * gameRatio) + "px";
        canvas.style.height = windowHeight + "px";
    }
}